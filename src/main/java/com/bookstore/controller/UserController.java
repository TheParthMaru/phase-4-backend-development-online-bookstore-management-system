package com.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.bookstore.model.Users;
import com.bookstore.service.UserServices;

@Controller
public class UserController {

	@Autowired
	UserServices userService;
	
	@GetMapping("user-registration")
	public String showUserRegistrationPage(Model m) {
		m.addAttribute("user", new Users());
		return "user-registration";
	}
	
	@PostMapping("add-user")
	public String addUser(@ModelAttribute("user") Users user) {
		userService.saveUser(user);
		
		return "success";
	}
	
	@GetMapping("view-users")
	public String viewUsers(Model m) {
		m.addAttribute("users", userService.findByAll());
		return "view-users";
	}
	
	@GetMapping("delete-user/{id}")
	public String deleteuser(@PathVariable String id) {
		userService.deleteById(id);
		return "redirect:/view-users";
	}
	
	@GetMapping("user-operations")
	public String showUserPage(Model m) {
		m.addAttribute("user", new Users());
		return "user-operations";
	}
	
}
