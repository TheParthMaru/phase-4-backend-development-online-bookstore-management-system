package com.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.bookstore.model.Admin;
import com.bookstore.model.Users;
import com.bookstore.service.AdminService;

@Controller
public class AdminController {

	@Autowired
	AdminService adminService;

	@GetMapping("/")
	public String welcome() {
		return "index";
	}

	@GetMapping("admin-operations")
	public String showAdminLoginPage(Model m) {
		m.addAttribute("admin", new Admin());
		return "admin-operations";
	}
}
