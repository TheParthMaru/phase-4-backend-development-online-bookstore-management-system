package com.bookstore.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bookstore.model.Books;
import com.bookstore.service.BooksService;

@Controller
public class BooksController {

	@Autowired
	BooksService bookService;
	
	@GetMapping("/view-books")
	public String showViewBooksPage(Model m) {
		m.addAttribute("books", bookService.findAll());
			return "view";			
	}

	@RequestMapping("/add-book")
	public String showAddBookPage(Model m) {
		m.addAttribute("books", new Books());
		return "insert";
	}

	@RequestMapping(value="/add-book", method=RequestMethod.POST)
	public String addBook(@ModelAttribute("books") Books books, Model m) {
		books.setPublishedDate(new Date(books.getPublishedDate().getTime()));
		bookService.saveBook(books);
		return "redirect:/view-books";
	}
	
	@RequestMapping(value = "/update-book/{id}")
	public String showUpdateBookPage(@PathVariable int id, Model m) {
		m.addAttribute("id", id);
		m.addAttribute("books", bookService.findById(id).orElse(null));
		return "update";
	}
	
	@RequestMapping(value = "/update-book/{id}", method = RequestMethod.POST)
	public String updateBook(@PathVariable int id, @ModelAttribute("books") Books books) {
		books.setPublishedDate(new Date(books.getPublishedDate().getTime()));
		bookService.updateBook(id, books);
		return "redirect:/view-books";
	}
	
	@RequestMapping(value = "/delete-book/{id}")
	public String deleteBook(@PathVariable int id) {
		bookService.deleteById(id) ;
		return "redirect:/view-books";
	}
}
