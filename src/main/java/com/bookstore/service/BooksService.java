package com.bookstore.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.model.Books;
import com.bookstore.repository.IBooksRepository;

@Service
public class BooksService {
	@Autowired
	private IBooksRepository booksRepository;


	public List<Books> findAll() {
		return booksRepository.findAll();
	}

	public Optional<Books> findById(int id) {
		return booksRepository.findById(id);
	}

	public Books saveBook(Books book) {
		return booksRepository.save(book);
	}

	public Books updateBook(int id, Books books) {
		Books updatedBook = booksRepository.findById(id).orElse(null);
		updatedBook.setBookName(books.getBookName());
		updatedBook.setAuthor(books.getAuthor());
		updatedBook.setEdition(books.getEdition());
		updatedBook.setPublishedDate(books.getPublishedDate());
		updatedBook.setPrice(books.getPrice());
		return booksRepository.save(updatedBook);
	}

	public void deleteById(int id) {
		booksRepository.deleteById(id);
	}
}
