package com.bookstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.model.Users;
import com.bookstore.repository.IUsersRepository;

@Service
public class UserServices {
	
	@Autowired
	private IUsersRepository userRepository;
	
	// Inserting user
	public Users saveUser(Users user) {
		return userRepository.save(user);
	}
	
	// Viewing users
	public List<Users> findByAll() {
		return userRepository.findAll();
	}
	
	// Delete Users
	public void deleteById(String email) {
		userRepository.deleteById(email);
	}
}
