package com.bookstore.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Users {

	@Id
	private String email;
	private String name;
	private String password;
	private String address;
	private Long mobile;

	public Users() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Users(String email, String name, String password, String address, Long mobile) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
		this.address = address;
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

}
