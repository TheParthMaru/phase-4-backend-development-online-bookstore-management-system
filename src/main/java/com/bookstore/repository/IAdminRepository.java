package com.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.model.Admin;

public interface IAdminRepository extends JpaRepository<Admin, String> {

}
