package com.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.model.Users;

public interface IUsersRepository extends JpaRepository<Users, String> {

}
