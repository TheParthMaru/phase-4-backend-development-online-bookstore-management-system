package com.bookstore.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.bookstore.controller", "com.bookstore.service"})
@EnableJpaRepositories("com.bookstore.repository")
@EntityScan("com.bookstore.model")
public class OnlineBookStoreManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineBookStoreManagementSystemApplication.class, args);
	}

}
