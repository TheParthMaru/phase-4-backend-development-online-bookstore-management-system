<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration</title>
<!-- Importing Custom CSS -->
<link rel="stylesheet" href="css/style.css" type="text/css">
<!-- Importing Bootstrap -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-success bg-success">
		<div class="container-fluid">
			<a href="/" class="navbar-brand text-decoration-none text-light">Online
				Book Store Management System</a>
		</div>
		<div
			class="container-fluid collapse navbar-collapse justify-content-end"
			id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link active text-decoration-none text-warning fw-bold"
					aria-current="page" href="/">Back To Home</a></li>
		</div>
	</nav>

	<img src="images/background-image.jpg"
		class="img-fluid blur background-img" alt="...">

	<div class="bg-text">
		<h1>User Registration</h1>

		<f:form action="add-user" method="post" modelAttribute="user"
			class="form">
			<table border="2"
				class="table-light text-light table-bordered border-light"
				cellspacing="5">
				<tr>
					<td>Name</td>
					<td><f:input path="name" /></td>
				</tr>

				<tr>
					<td>Email</td>
					<td><f:input path="email" /></td>
				</tr>

				<tr>
					<td>Password</td>
					<td><f:input path="password" /></td>
				</tr>

				<tr>
					<td>Address</td>
					<td><f:input path="address" /></td>
				</tr>

				<tr>
					<td>Mobile</td>
					<td><f:input path="mobile" /></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Add User"></td>
				</tr>
			</table>

		</f:form>
	</div>

</body>
</html>