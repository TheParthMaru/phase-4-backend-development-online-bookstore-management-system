<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Operations</title>
<!-- Importing Custom CSS -->
<link rel="stylesheet" href="css/style.css" type="text/css">
<!-- Importing Bootstrap -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-success bg-success">
		<div class="container-fluid">
			<a href="/" class="navbar-brand text-decoration-none text-light">Online
				Book Store Management System</a>
		</div>
		<div
			class="container-fluid collapse navbar-collapse justify-content-end"
			id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link active text-decoration-none text-warning fw-bold"
					aria-current="page" href="admin-operations">Admin Operations</a></li>
				<li class="nav-item"><a
					class="nav-link text-decoration-none text-light" href="view-users">View
						Users</a></li>

				<li class="nav-item"><a
					class="nav-link text-decoration-none text-light"
					href="/add-book">Add Books</a></li>

				<li class="nav-item"><a
					class="nav-link text-decoration-none text-light" href="/view-books">View
						Books</a></li>
		</div>
	</nav>
	
	<img src="images/background-image.jpg"
		class="img-fluid blur background-img" alt="...">

	<div class="bg-text">
		<h1>Welcome Admin</h1>
	</div>
</body>
</html>