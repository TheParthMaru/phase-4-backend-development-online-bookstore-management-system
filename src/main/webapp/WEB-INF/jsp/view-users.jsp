<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Users</title>

<!-- Importing Custom CSS -->
<link rel="stylesheet" href="css/style.css" type="text/css">
<!-- Importing Bootstrap -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-success bg-success">
		<div class="container-fluid">
			<a href="/" class="navbar-brand text-decoration-none text-light">Online
				Book Store Management System</a>
		</div>
		<div
			class="container-fluid collapse navbar-collapse justify-content-end"
			id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link active text-decoration-none text-warning fw-bold"
					aria-current="page" href="admin-operations">Back To Admin
						Operations</a></li>

		</div>
	</nav>

	<img src="images/background-image.jpg"
		class="img-fluid blur background-img" alt="...">

	<div class="bg-text">
		<h1>Users</h1>
		<table border="2"
			class="table-light text-light table-bordered border-light"
			cellspacing="5">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Password</th>
				<th>Address</th>
				<th>Mobile Number</th>
				<th>Delete</th>
			</tr>
			<c:forEach var="user" items="${users}">
				<tr>
					<td>${user.getName()}</td>
					<td>${user.getEmail()}</td>
					<td>${user.getPassword()}</td>
					<td>${user.getAddress()}</td>
					<td>${user.getMobile()}</td>
					
					<td><a href="delete-user/${user.getEmail()}" id="danger"
						class="text-decoration-none fst-italic">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>