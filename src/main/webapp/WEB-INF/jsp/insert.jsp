<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Book</title>
<!-- Importing Custom CSS -->
<link rel="stylesheet" href="css/style.css" type="text/css">
<!-- Importing Bootstrap -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-success bg-success">
		<div class="container-fluid">
			<a href="/" class="navbar-brand text-decoration-none text-light">Online
				Book Store Management System</a>
		</div>
		<div
			class="container-fluid collapse navbar-collapse justify-content-end"
			id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item"><a
					class="nav-link active text-decoration-none text-warning fw-bold"
					aria-current="page" href="admin-operations">Back To Admin
						Operations</a></li>
		</div>
	</nav>

	<img src="images/background-image.jpg"
		class="img-fluid blur background-img" alt="...">

	<div class="bg-text">
		<h1>Add Books</h1>
		<f:form method="post" action="/add-book" modelAttribute="books"
			class="form">
			<table border="2"
				class="table-light text-light table-bordered border-light"
				cellspacing="5">
				<tr>
					<th>Book Id</th>
					<td><f:input path="bookId" /></td>
				</tr>
				<tr>
					<th>Book Name</th>
					<td><f:input path="bookName" /></td>
				</tr>

				<tr>
					<th>Author</th>
					<td><f:input path="author" /></td>
				</tr>

				<tr>
					<th>Edition</th>
					<td><f:input path="edition" /></td>
				</tr>

				<tr>
					<th>Published Date</th>
					<td><f:input path="publishedDate" /></td>
				</tr>

				<tr>
					<th>Price</th>
					<td><f:input path="price" /></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Add Book" class="success" /></td>
				</tr>
			</table>
		</f:form>
	</div>

</body>
</html>